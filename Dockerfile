#FROM java:8
FROM openjdk:8-jdk-alpine
EXPOSE 8083
ARG JAR_FILE=target/dockerdemo.war
WORKDIR /opt/app
COPY ${JAR_FILE} dockerdemo.war
ADD /target/dockerdemo.war dockerdemo.war
ENTRYPOINT ["java", "-jar", "dockerdemo.war"]
